<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttention extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient');
            $table->integer('sessions');
            $table->dateTime('startdate');
            $table->enum('interval', ['Lu-Ma-Mi-Ju-Vi-Sa','Lu-Ma-Mi-Ju-Vi', 'Lu-Mi-Vi', 'Ma-Ju-Sa']);
            $table->time('hour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attention');
    }
}
