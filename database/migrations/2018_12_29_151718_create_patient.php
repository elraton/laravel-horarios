<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient', function (Blueprint $table) {
            $table->increments('id');
            $table->string('surnames');
            $table->string('names');
            $table->enum('type', ['Particular','Seguro EPS', 'SOAT']);
            $table->boolean('traumatologist')->nullable();
            $table->boolean('physiatrist')->nullable();
            $table->boolean('operation')->nullable();
            $table->boolean('fracture')->nullable();
            $table->boolean('accident')->nullable();
            $table->dateTime('medic_attention')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient');
    }
}
