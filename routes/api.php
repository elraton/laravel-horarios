<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//Route::middleware('api')->group(function () {
//Route::group(['prefix' => 'v1', 'middleware' => 'cors'], function() {
Route::middleware('api')->group(function () {
    //Patients
    Route::get('all_patients', 'patientController@all');
    Route::post('get_patients', 'patientController@index');
    Route::post('store_patient', 'patientController@store');
    Route::post('update_patient', 'patientController@update');
    Route::post('destroy_patient', 'patientController@destroy');

    //Personals
    Route::get('get_personals', 'PersonalController@index');
    Route::post('store_personal', 'PersonalController@store');
    Route::post('update_personal', 'PersonalController@update');
    Route::post('destroy_personal', 'PersonalController@destroy');

    //Personal Schedule
    Route::get('get_all_personal_schedules', 'Personal_ScheduleController@getall');
    Route::post('get_personal_schedules', 'Personal_ScheduleController@index');
    Route::post('store_personal_schedule', 'Personal_ScheduleController@store');
    Route::post('update_personal_schedule', 'Personal_ScheduleController@update');
    Route::post('destroy_personal_schedule', 'Personal_ScheduleController@destroy');

    //Pending Board
    Route::get('get_pending_boards', 'pendingController@index');
    Route::post('search_pending_boards', 'pendingController@search');
    Route::post('store_pending_board', 'pendingController@store');
    Route::post('update_pending_board', 'pendingController@update');
    Route::post('destroy_pending_board', 'pendingController@destroy');

    //Calendar Days
    Route::post('get_one_calendar', 'calendarDayController@getone');
    Route::post('get_calendar_attention', 'calendarDayController@getbyAttention');
    Route::post('get_calendar_days', 'calendarDayController@index');
    Route::post('store_calendar_day', 'calendarDayController@store');
    Route::post('update_calendar_day', 'calendarDayController@update');
    Route::post('destroy_calendar_day', 'calendarDayController@destroy');
    Route::post('get_calendar_by_global', 'calendarDayController@getbyGlobal');

    //Global Attention
    Route::get('get_global_attentions', 'globalAttentionController@index');
    Route::post('get_global_attention', 'globalAttentionController@getbyPatient');
    Route::post('store_global_attention', 'globalAttentionController@store');
    Route::post('update_global_attention', 'globalAttentionController@update');
    Route::post('destroy_global_attention', 'globalAttentionController@destroy');
    
    //Terapist Attentions
    Route::post('get_attention', 'attentionController@getAttention');
    Route::post('get_attention_patient', 'attentionController@getAttention_by_patient');
    Route::post('get_attention_personal', 'attentionController@getAttention_by_personal');
    Route::get('get_attentions', 'attentionController@index');
    Route::post('store_attention', 'attentionController@store');
    Route::post('update_attention', 'attentionController@update');
    Route::post('destroy_attention', 'attentionController@destroy');

    //Doctor Attentions
    Route::get('get_doctor_attentions', 'doctorAttentionController@index');
    Route::post('get_doctor_attention_range', 'doctorAttentionController@getbyRange');
    Route::post('get_doctor_attention_patient', 'doctorAttentionController@getbyPatient');
    Route::post('get_doctor_attention_personal', 'doctorAttentionController@getbyPersonal');    
    Route::post('store_doctor_attention', 'doctorAttentionController@store');
    Route::post('update_doctor_attention', 'doctorAttentionController@update');
    Route::post('destroy_doctor_attention', 'doctorAttentionController@destroy');

    //Users
    Route::post('login', 'UsersFvController@Auth');
    Route::post('getuser', 'UsersFvController@getOne');
    Route::post('save_user', 'UsersFvController@save');
    Route::post('update_user', 'UsersFvController@update');
    Route::post('destroy_user', 'UsersFvController@destroy');

});