<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\doctorAttention;

class doctorAttentionController extends Controller
{
    public function index(Request $request)
    {
        $data = doctorAttention::all();
        return response()->json(['data' => $data], 200);
    }
    public function getbyRange(Request $request)
    {
        $data = doctorAttention::where('schedule', '>', $request->get('from'))->where('schedule', '<', $request->get('to'))->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }
    public function getbyPatient(Request $request) {
        $data = doctorAttention::where('patient', '=', $request->get('patient'));

        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }
    public function getbyPersonal(Request $request) {
        $data = doctorAttention::where('personal', '=', $request->get('personal'));

        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }
    public function store(Request $request)
    {
        $attention = new doctorAttention;
        $attention->patient = $request->get('patient');
        $attention->personal = $request->get('personal');
        $attention->sede = $request->get('sede');
        $attention->schedule = $request->get('schedule');
        $attention->global = $request->get('global');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function update(Request $request)
    {
        $attention = doctorAttention::find($request->id);
        $attention->patient = $request->get('patient');
        $attention->personal = $request->get('personal');
        $attention->sede = $request->get('sede');
        $attention->schedule = $request->get('schedule');
        $attention->global = $request->get('global');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }
    public function destroy($id)
    {
        $delete = doctorAttention::destroy($id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
