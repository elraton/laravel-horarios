<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pending;

class pendingController extends Controller
{
    public function index()
    {
        $data = Pending::all();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function store(Request $request)
    {
        $aux = Pending::where('patient', '=', $request->get('patient'))->where('attention', '=', $request->get('attention'))->first();
        if (!$aux) {
            $attention = new Pending;
            $attention->patient = $request->get('patient');
            $attention->attention = $request->get('attention');
            $attention->message = $request->get('message');
            $attention->level = $request->get('level');
            $attention->user = $request->get('user');

            $saved = $attention->save();
            if (!$saved){
                return response()->json(['data' => 'fail'], 500);
            }
            return response()->json(['data' => 'ok'], 200);
        } else {
            return response()->json(['data' => 'ok'], 200);
        }
    }

    public function update(Request $request)
    {
        $attention = Pending::find($request->id);
        $attention->patient = $request->get('patient');
        $attention->attention = $request->get('attention');
        $attention->message = $request->get('message');
        $attention->level = $request->get('level');
        $attention->user = $request->get('user');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function search(Request $request)
    {
        $attention = Pending::where('attention', '=', $request->attention)->where('patient', '=', $request->patient)->first();
        $delete = $attention->delete();
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }

    public function destroy($id)
    {
        $delete = Pending::destroy($id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
