<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalendarDay;
use App\Attention;

class calendarDayController extends Controller
{

    public function getbyGlobal(Request $request)
    {
        $attention = Attention::where('global', '=', $request->global)->first();
        $data = CalendarDay::where('attention', '=', $attention->id)->orderBy('schedule')->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function getone(Request $request)
    {
        $data = CalendarDay::find($request->id);
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }
    public function index(Request $request)
    {
        $data = CalendarDay::where('schedule', '>', $request->get('from'))->where('schedule', '<', $request->get('to'))->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function getbyAttention(Request $request)
    {
        $data = CalendarDay::where('attention', '=', $request->get('attention'))->orderBy('schedule', 'asc')->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function store(Request $request)
    {
        $attention = new CalendarDay;
        $attention->attention = $request->get('attention');
        $attention->patient = $request->get('patient');
        $attention->status = $request->get('status');
        $attention->comments = $request->get('comments');
        $attention->schedule = $request->get('schedule');
        $attention->personal = $request->get('personal');
        $attention->session = $request->get('session');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function update(Request $request)
    {
        $attention = CalendarDay::find($request->id);
        $attention->attention = $request->get('attention');
        $attention->patient = $request->get('patient');
        $attention->status = $request->get('status');
        $attention->comments = $request->get('comments');
        $attention->schedule = $request->get('schedule');
        $attention->personal = $request->get('personal');
        $attention->session = $request->get('session');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function destroy($id)
    {
        $delete = CalendarDay::destroy($id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
