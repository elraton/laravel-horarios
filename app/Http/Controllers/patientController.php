<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;

class patientController extends Controller
{
    //
    public function index(Request $request)
    {
        $data = Patient::where('medic_attention', '>', $request->get('from'))->where('medic_attention', '<', $request->get('to'))->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function all()
    {
        $data = Patient::all();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function store(Request $request)
    {
        $patient = new Patient;
        $patient->surnames = $request->get('surnames');
        $patient->names = $request->get('names');
        $patient->dni = $request->get('dni');
        $patient->type = $request->get('type');
        $patient->traumatologist = $request->get('traumatologist');
        $patient->physiatrist = $request->get('physiatrist');
        $patient->operation = $request->get('operation');
        $patient->fracture = $request->get('fracture');
        $patient->accident = $request->get('accident');
        $patient->doctor = $request->get('doctor');
        $patient->medic_attention = $request->get('medic_attention');

        $saved = $patient->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function update(Request $request)
    {
        $patient = Patient::find($request->id);

        $patient->surnames = $request->get('surnames');
        $patient->names = $request->get('names');
        $patient->dni = $request->get('dni');
        $patient->type = $request->get('type');
        $patient->traumatologist = $request->get('traumatologist');
        $patient->physiatrist = $request->get('physiatrist');
        $patient->operation = $request->get('operation');
        $patient->fracture = $request->get('fracture');
        $patient->accident = $request->get('accident');
        $patient->doctor = $request->get('doctor');
        $patient->medic_attention = $request->get('medic_attention');

        $saved = $patient->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function destroy($id)
    {
        $delete = Patient::destroy($id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
