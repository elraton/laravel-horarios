<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\UsersFv;
use Illuminate\Support\Facades\Hash;

class UsersFvController extends Controller
{
    public function getOne(Request $request)
    {
        $data = UsersFv::where('username', '=', $request->get('username'))->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function Auth(Request $request)
    {
        $data = UsersFv::where('username', '=', $request->get('username'))->first();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        if ($data->password == Hash::check($request->get('password'), $data->password) ) {
            return response()->json(['data' => $data], 200);
        } else {
            return response()->json(['data' => ''], 500);
        }
    }

    public function save(Request $request)
    {
        $user = new UsersFv;
        $user->username = $request->get('username');
        $user->password = Hash::make($request->get('password'));
        $user->sede = $request->get('sede');
        $user->rol = $request->get('rol');

        $saved = $user->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function update(Request $request)
    {
        $user = UsersFv::find($request->id);
        $user->username = $request->get('username');
        $user->password = Hash::make($request->get('password'));
        $user->sede = $request->get('sede');
        $user->rol = $request->get('rol');

        $saved = $user->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function destroy($id)
    {
        $delete = UsersFv::destroy($id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
