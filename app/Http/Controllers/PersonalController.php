<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personal;
use App\Personal_Schedule;

class PersonalController extends Controller
{
    public function index()
    {
        $data = Personal::all();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function store(Request $request)
    {
        $patient = new Personal;
        $patient->surname = $request->get('surname');
        $patient->name = $request->get('name');
        $patient->dni = $request->get('dni');
        $patient->is_doctor = $request->get('is_doctor');
        $patient->is_kids = $request->get('is_kids');
        $patient->sede = $request->get('sede');

        $saved = $patient->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function update(Request $request)
    {
        $patient = Personal::find($request->id);

        $patient->surname = $request->get('surname');
        $patient->name = $request->get('name');
        $patient->dni = $request->get('dni');
        $patient->is_doctor = $request->get('is_doctor');
        $patient->is_kids = $request->get('is_kids');
        $patient->sede = $request->get('sede');

        $saved = $patient->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function destroy(Request $request)
    {
        $delete = Personal::destroy($request->id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
