<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attention;
use App\CalendarDay;

class attentionController extends Controller
{
    //
    public function index()
    {
        $data = Attention::all();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function getAttention_by_patient(Request $request)
    {
        $data = Attention::where('patient', '=', $request->id)->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function getAttention_by_personal(Request $request)
    {
        $data = Attention::where('personal', '=', $request->id)->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function getAttention(Request $request)
    {
        $data = Attention::find($request->id);
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function store(Request $request)
    {
        $attention = new Attention;
        $attention->patient = $request->get('patient');
        $attention->sessions = $request->get('sessions');
        $attention->startdate = $request->get('startdate');
        $attention->interval = $request->get('interval');
        $attention->hour = $request->get('hour');
        $attention->hour2 = $request->get('hour2');
        $attention->personal = $request->get('personal');
        $attention->global = $request->get('global');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        
        foreach ($request->get('schedule') as $value) {
            $calendar = new CalendarDay;
            $calendar->attention = $attention->id;
            $calendar->patient = $request->get('patient');
            $calendar->status = 'vino';
            $calendar->comments = '';
            $calendar->schedule = $value;
            $calendar->personal = $request->get('personal');
            $calendar->session = $request->get('sessions');
            $calendar->save();
        }
        
        return response()->json(['data' => 'ok'], 200);
    }

    public function update(Request $request)
    {
        $attention = Attention::find($request->id);
        $attention->patient = $request->get('patient');
        $attention->sessions = $request->get('sessions');
        $attention->startdate = $request->get('startdate');
        $attention->interval = $request->get('interval');
        $attention->hour = $request->get('hour');
        $attention->hour2 = $request->get('hour2');
        $attention->personal = $request->get('personal');
        $attention->global = $request->get('global');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        $calendars = CalendarDay::where('attention', '=', $attention->id)->where('schedule','>=',$request->get('startdate2'))->delete();
        
        foreach ($request->get('schedule') as $value) {
            $calendar = new CalendarDay;
            $calendar->attention = $attention->id;
            $calendar->patient = $request->get('patient');
            $calendar->status = 'vino';
            $calendar->comments = '';
            $calendar->schedule = $value;
            $calendar->personal = $request->get('personal');
            $calendar->session = $request->get('sessions');
            $calendar->save();
        }
        
        return response()->json(['data' => 'ok'], 200);
    }

    public function destroy($id)
    {
        $delete = Attention::destroy($id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
