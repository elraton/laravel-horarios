<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\globalAttention ;

class globalAttentionController extends Controller
{
    public function index(Request $request) {
        $data = globalAttention::all();

        return response()->json(['data' => $data], 200);
    }

    public function getbyPatient(Request $request) {
        $data = globalAttention::where('patient', '=', $request->get('patient'))->where('status', '=', 'active')->latest()->first();

        return response()->json(['data' => $data], 200);
    }

    public function store(Request $request)
    {
        $attention = new globalAttention;
        $attention->patient = $request->get('patient');
        $attention->status = $request->get('status');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => $attention->id], 200);
    }

    public function update(Request $request)
    {
        $attention = globalAttention::find($request->id);
        $attention->patient = $request->get('patient');
        $attention->status = $request->get('status');

        $saved = $attention->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }
    public function destroy($id)
    {
        $delete = globalAttention::destroy($id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
