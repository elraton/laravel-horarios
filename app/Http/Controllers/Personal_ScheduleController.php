<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personal_Schedule;

class Personal_ScheduleController extends Controller
{
    public function index(Request $request)
    {
        $data = Personal_Schedule::where('personal', '=', $request->get('id'))->get();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }

    public function getall()
    {
        $data = Personal_Schedule::all();
        if (!$data) {
            return response()->json(['data' => ''], 500);
        }
        return response()->json(['data' => $data], 200);
    }
    public function store(Request $request)
    {

        $patient = new Personal_Schedule;
        $patient->personal = $request->get('personal');
        $patient->day = $request->get('day');
        $patient->start_hour = $request->get('start_hour');
        $patient->end_hour = $request->get('end_hour');
        $patient->sede = $request->get('sede');
        $patient->duration = $request->get('duration');

        $saved = $patient->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function update(Request $request)
    {
        $patient = Personal_Schedule::find($request->id);

        $patient->personal = $request->get('personal');
        $patient->day = $request->get('day');
        $patient->start_hour = $request->get('start_hour');
        $patient->end_hour = $request->get('end_hour');
        $patient->sede = $request->get('sede');
        $patient->duration = $request->get('duration');

        $saved = $patient->save();
        if (!$saved){
            return response()->json(['data' => 'fail'], 500);
        }
        return response()->json(['data' => 'ok'], 200);
    }

    public function destroy(Request $request)
    {
        $delete = Personal_Schedule::destroy($request->id);
        if (!$delete) {
            return response()->json(['data' => 'fail'], 500);
        }

        return response()->json(['data' => 'ok'], 200);
    }
}
